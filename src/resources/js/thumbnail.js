/**
 * Created by Jesse Kramer on 05/02/2016.
 */
window.addEventListener("keyup", changeHandler);
window.addEventListener("mousedown", changeHandler);

function changeHandler(event) {
    var id = event.target.id;
    var img = document.getElementById("result_image");
    var link = document.getElementById("link");

    if (id==="url") {
        var url = event.target.value;
        var id = getYouTubeID(url);
        if (id) {
            img.src = getImgURL(id);
            link.href = getImgURL(id);
        }
    }
}

function getYouTubeID(url){
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false ;
}

function getImgURL(id) {
    return "http://img.youtube.com/vi/"+id+"/maxresdefault.jpg";
}